#!/usr/bin/python

import serial
from time import sleep
import subprocess
import base64
import zlib
from kiss import Encoder
from kiss import Decoder

ser = serial.Serial("/dev/ttyAMA0",115200)
photoEncoder = Encoder()

cmdTrigger = "takeLDphoto"
cmdTriggerHD = "takeHDphoto"
cmdPhoto = "raspistill -w 640 -h 480 -q 50 -o SerialImage.jpg"
cmdPhotoHD = "raspistill -w 1080 -h 720 -q 50 -o SerialImage.jpg"

while True:

	cmdHD = False
	cmdNotRx = True

	print("waiting for command...")
	while cmdNotRx:
		serialRx = ser.read()
		sleep(0.05)
		serialLeft = ser.inWaiting()
		serialRx += ser.read(serialLeft)
		print(serialRx)

		#if True:
		if cmdTrigger in serialRx:
			cmdNotRx = False
			cmdHD = False
			print("LD command received")
		elif cmdTriggerHD in serialRx:
			cmdNotRx = False
			cmdHD = True
			print("HD command received")

	print("taking photo")
	if cmdHD:
		subprocess.call(cmdPhotoHD, shell=True)
	else:
		subprocess.call(cmdPhoto, shell=True)
	print("photo taken")

	sleep(1)

	print("compressing...")
	with open("SerialImage.jpg","rb") as imageFile:
		photoStr = base64.b64encode(imageFile.read())

	compressedStr = photoStr

	data_packs = photoEncoder.apply(compressedStr)
	print("sending...")
	ser.write(data_packs)

	sleep(3)
